using UnityEngine;

public class DragRigidbodies : MonoBehaviour
{


    public GameObject mouseObject;



    public float forceAmount = 500;
    public LayerMask hitMask;
    SpringJoint selectedJoint;
    private Rigidbody selectedRigid;
    Camera targetCamera;
    Vector3 originalScreenTargetPosition;
    Vector3 originalRigidbodyPos;
    float selectionDistance;

    private GameObject currentSelected;

    public Match3dDrop dropElement;
    // Start is called before the first frame update



    void Start()
    {
        targetCamera = GetComponent<Camera>();
    }

    public void ReleaseObject()
    {
        if (selectedJoint)
        {
            selectedJoint.spring = 0f;
            selectedJoint.connectedBody = null;
        }

        if (selectedRigid)
        {
            selectedRigid.drag = 1f;
            selectedRigid.angularDrag = 1f;

        }

        selectedRigid = null;
        Destroy(selectedJoint);
    }

    void Update()
    {
        if (!targetCamera)
            return;


        if (Input.GetMouseButtonDown(0))
        {
            
            //Check if we are hovering over Rigidbody, if so, select it
            ClickItem();
        }

        if (Input.GetMouseButtonUp(0) && selectedJoint)
        {
            //Release selected Rigidbody if there any
            ReleaseObject();
        }



    }

    void FixedUpdate()
    {
        if (selectedJoint)
        {
            if(selectedJoint.GetComponent<Match3DItem>().inDropZone)
                dropElement.DisableColliderShort();
            selectedRigid.isKinematic = false;
            selectedJoint.connectedBody = mouseObject.GetComponent<Rigidbody>();
            selectedJoint.spring = 20f;
            selectedJoint.autoConfigureConnectedAnchor = false;
            selectedJoint.anchor = Vector3.zero;
            selectedJoint.connectedAnchor = Vector3.zero;
            selectedRigid.drag = 4f;
            selectedRigid.angularDrag = 4f;
            //Debug.Log(selectedJoint.gameObject.name);
        }
    }

    void ClickItem()
    {
        RaycastHit hitInfo = new RaycastHit();
        Ray ray = targetCamera.ScreenPointToRay(Input.mousePosition);
        bool hit = Physics.Raycast(ray, out hitInfo, 100f, hitMask);
        if (hit)
        {
            if (hitInfo.collider.gameObject.GetComponent<Match3DItem>())
            {
                currentSelected = hitInfo.collider.gameObject;
                selectionDistance = Vector3.Distance(ray.origin, hitInfo.point);
                originalScreenTargetPosition = targetCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, selectionDistance));
                originalRigidbodyPos = hitInfo.collider.transform.position;
                selectedRigid = currentSelected.GetComponent<Rigidbody>();
                selectedJoint = currentSelected.AddComponent<SpringJoint>();
            }
        }

     
    }
}