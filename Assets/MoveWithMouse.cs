using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithMouse : MonoBehaviour
{

    [SerializeField] int fixedHeight = 1;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    private void Update()
    {

        Plane plane = new Plane(Vector3.up, Vector3.up * fixedHeight); // ground plane

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float distance; // the distance from the ray origin to the ray intersection of the plane
        if (plane.Raycast(ray, out distance))
        {
            Vector3 rayPoint = ray.GetPoint(distance);
            transform.position = Vector3.SmoothDamp(transform.position, rayPoint, ref velocity, smoothTime);
        }
    }
}
