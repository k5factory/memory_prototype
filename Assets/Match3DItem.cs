using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIEffects;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Match3DItem : MonoBehaviour
{

    public enum MainCategory { Food, Music, Love, Living, Fashion };

    public MainCategory mainCategory;



    [Header("References")]
    public bool memory = false;
    public Image[] mainImages;
    private Vector3 startPos;

    [Header("Settings")]
    public int id;
    public string nameItem;

    public Sprite icon;

    [Header("Controls")]
    public bool canBeClicked = true;
    public bool isClicked = false;
    public bool inDropZone = false;


    private void Start()
    {
        GetComponent<Rigidbody>().drag = 1f;
        GetComponent<Rigidbody>().angularDrag = 1f;
    }

    public void SetImage(Sprite main)
    {
        mainImages = GetComponentsInChildren<Image>();
        foreach (var mainImage in mainImages)
        {
            if (mainImage)
                mainImage.sprite = main;
        }
    }




}
