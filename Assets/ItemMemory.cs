using System;
using System.Collections;
using Coffee.UIEffects;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ItemMemory : ClickBase
{

    public enum MainCategory { Food, Music, Love, Living, Fashion };

    public MainCategory mainCategory;

    [Header("Controls")]
    public ItemMemory previousClicked;

    [Header("References")]
    public Image mainImage;
    public Image backImage;
    public bool memory = false;
    public ParticleSystem winParticle;
    public UIDissolve[] allDissolves;
    [CanBeNull] public UIShiny goldEdge;

    private Vector3 startPos;

    public override void Start()
    {
        base.Start();
        mainImage.gameObject.SetActive((true));
        goldEdge.gameObject.SetActive((false));
        backImage.gameObject.SetActive(false);
        winParticle.gameObject.SetActive((false));

    }

    public void SetImage(Sprite main)
    {
        if (mainImage)
            mainImage.sprite = main;
    }

    public void SwitchCardBack()
    {
        LeanTween.rotateLocal(this.gameObject, new Vector3(0, 0, 0), 0.2f).setEaseOutBounce().setOnUpdate((Action<Vector3>)SwitchCardBackUpdate);

    }

    private void SwitchCardBackUpdate(Vector3 obj)
    {
        //print(obj.y);
        if (obj.y >= 270f || obj.y <= 90f && !backImage.gameObject.activeSelf)
        {

            mainImage.gameObject.SetActive((false));
            backImage.gameObject.SetActive((true));
        }

    }

    private void SwitchCardFrontUpdate(Vector3 obj)
    {
        //print(obj.y);
        if (obj.y <= -90f || obj.y >= 90f && !mainImage.gameObject.activeSelf)
        {

            mainImage.gameObject.SetActive((true));
            backImage.gameObject.SetActive((false));
        }

    }

    public void SwitchCardFront()
    {
        LeanTween.rotateLocal(this.gameObject, new Vector3(0, 180, 0), 0.2f).setEaseOutBounce().setOnUpdate((Action<Vector3>)SwitchCardFrontUpdate);

    }

    public override void ResetItem()
    {
        memory = false;
        previousClicked = null;
        mainImage.gameObject.SetActive((true));
        backImage.gameObject.SetActive((false));
        ResetDissolve();
        DeselectItem();
    }

    public override void SelectItem()
    {
        startPos = transform.localPosition;
        //LeanTween.moveLocal(this.gameObject, new Vector3(transform.localPosition.x, transform.localPosition.y, -20f), 0.5f).setOnComplete(SelectBounce).setEaseOutBounce();
        isClicked = true;
    }

    private void SelectBounce()
    {
        LeanTween.moveLocal(this.gameObject, startPos, 0.5f).setEaseOutBounce();
    }

    public override void DeselectItem()
    {
        isClicked = false;
    }

    private void OnEnable()
    {
        EventManager.MemoryClickDelegate += ItemClicked;
    }

    private void OnDisable()
    {
        EventManager.MemoryClickDelegate -= ItemClicked;
        //RESET
        //gameObject.GetComponent<Image>().color = Color.white;
        //isClicked = false;
    }

    private void ItemClicked(ItemMemory itemElement)
    {
        if (itemElement == this)
        {
            if (!isClicked)
            {
                SelectItem();
                if (nameText != null)
                    nameText.text = nameItem;
                SwitchCardFront();
                if (previousClicked != null)
                {
                    // Check if same ID
                    if (previousClicked.id == id && previousClicked.isClicked)
                    {
                        CorrectConnection();
                        previousClicked.CorrectConnection();
                        previousClicked.memory = true;
                        memory = true;
                        MemoryPrototype.Instance.CorrectSelection(this, previousClicked);
                    }
                    else if (previousClicked.id != id && previousClicked.isClicked)
                    {
                        WrongConnection();
                        previousClicked.WrongConnection();
                        MemoryPrototype.Instance.WrongSelection();
                    }
                }
            }
            else
            {
                DeselectItem();
            }

        }
        else
        {
            // PLAY PHASE, if same ID, same Element
            previousClicked = itemElement;
        }
    }

    public void WrongConnection()
    {
        StartCoroutine(_WrongConnection());
    }

    public void CorrectConnection()
    {
        StartCoroutine(_CorrectConnection());
    }

    public void FailedMemory()
    {
        StartCoroutine(_FailedMemory());
    }

    public void DissolveCard()
    {
        foreach (UIDissolve u in allDissolves)
        {
            u.Play();
        }
    }

    public void ResetDissolve()
    {
        foreach (UIDissolve u in allDissolves)
        {
            u.Stop(true);
            u.effectFactor = 0;
        }
    }
    IEnumerator _CorrectConnection()
    {
        if (winParticle)
            winParticle.Play();
        yield return new WaitForSeconds(0.2f);
        yield return new WaitForSeconds(1f);
        isClicked = false;
        canBeClicked = true;
        //gameObject.SetActive(false);
    }
    IEnumerator _WrongConnection()
    {
        yield return new WaitForSeconds(0.2f);
        canBeClicked = false;
        yield return new WaitForSeconds(1f);
        canBeClicked = true;
        isClicked = false;
        SwitchCardBack();
    }

    IEnumerator _FailedMemory()
    {
        yield return new WaitForSeconds(0.2f);
        SwitchCardBack();
        canBeClicked = false;
        yield return new WaitForSeconds(1f);
        isClicked = false;
        canBeClicked = true;
        DissolveCard();
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }


    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        if (gameObject.activeSelf && canBeClicked)
        {
            EventManager.OnMemoryClick(this);
        }
    }
}