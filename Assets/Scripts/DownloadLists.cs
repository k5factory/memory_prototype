using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadLists : MonoBehaviour
{
    public static DownloadLists Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    private void Start()
    {
        if (DBManager.LoggedIn)
        {
            DownloadItems();
            DownloadPlayers();
        }

    }

    public void DownloadItems()
    {
        StartCoroutine(_GetItemsDB());
    }

    public void DownloadPlayers()
    {
        StartCoroutine(_GetPlayersDB());
    }
    IEnumerator _GetItemsDB()
    {
        UnityWebRequest request = UnityWebRequest.Get(DBManager.url + "getitems.php");
        request.downloadHandler = new DownloadHandlerBuffer();
        yield return request.SendWebRequest();
        //Debug.Log(request.downloadHandler.text);
        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (File.Exists(DBManager.GetPath("items")))
            {
                File.Delete(DBManager.GetPath("items"));
                Debug.Log("Delete old Item List");
            }

            StreamWriter streamWriter = File.CreateText(DBManager.GetPath("items"));
            streamWriter.WriteLine(request.downloadHandler.text);
            streamWriter.Close();
            yield return streamWriter;
        }
    }

    IEnumerator _GetPlayersDB()
    {
        WWWForm form = new WWWForm();

        form.AddField("name", DBManager.username);

        UnityWebRequest request = UnityWebRequest.Post(DBManager.url + "getotherplayers.php", form);
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (File.Exists(DBManager.GetPath("players")))
            {
                File.Delete(DBManager.GetPath("players"));
                Debug.Log("Delete old Players List");
            }

            StreamWriter streamWriter = File.CreateText(DBManager.GetPath("players"));
            streamWriter.WriteLine(request.downloadHandler.text);
            streamWriter.Close();
            yield return streamWriter;
        
        }
    }
}
