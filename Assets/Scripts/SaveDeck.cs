using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SaveDeck : MonoBehaviour
{

    public void SaveDeckDB()
    {
        StartCoroutine(_SaveDeckDB());
    }

    public void PlayDeckOtherPlayer()
    {

    }
    IEnumerator _SaveDeckDB()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", DBManager.username);
        form.AddField("deck", DBManager.deckString);

        UnityWebRequest request = UnityWebRequest.Post(DBManager.url + "savedeck.php", form);
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (request.downloadHandler.text[0] == '0')
            {
                Debug.Log("Deck Saved");
                DeckCreation.Instance.SaveDeckFinish();
            }
            else
            {
                Debug.Log("FAILED SAVE DECK: " + DBManager.deckString);
                Debug.Log(request.downloadHandler.text);
            }
        }

    }
}
