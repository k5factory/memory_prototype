using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public static MainMenu Instance;
    [Header("References")]
    public TextMeshProUGUI loggedInText;
    public Login login;
    public Button registerButton;
    public Button loginButton;
    public Button deckBuilderButton;
    public Button multiplayerButton;
    public Button logoutButton;
    public Button playGameButton;
    [Header("Canvases")]
    public CanvasAlphaControl mainMenuCanvas;
    public CanvasAlphaControl loginCanvas;
    public CanvasAlphaControl registerCanvas;

    CanvasAlphaControl currentMenu;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }

    private void Start()
    {
        if (DBManager.LoggedIn)
            loggedInText.text = "Welcome: " + DBManager.username;

        CheckLogin();

        mainMenuCanvas.gameObject.SetActive(true);
        mainMenuCanvas.cGroup.alpha = 1;
        loginCanvas.gameObject.SetActive(false);
        if (loginCanvas.gameObject.activeSelf)
            loginCanvas.cGroup.alpha = 0;
        registerCanvas.gameObject.SetActive(false);
        if (registerCanvas.gameObject.activeSelf)
            registerCanvas.cGroup.alpha = 0;
    }

    public void SwitchCanvas(CanvasAlphaControl fromCanvas, CanvasAlphaControl toCanvas)
    {
        StartCoroutine(_SwitchCanvas(fromCanvas, toCanvas));
    }

    private void CheckLogin()
    {
        playGameButton.interactable = DBManager.deckIDsLocal.Count > 0;
        logoutButton.interactable = DBManager.LoggedIn;
        registerButton.interactable = !DBManager.LoggedIn;
        loginButton.interactable = !DBManager.LoggedIn;
        multiplayerButton.interactable = DBManager.LoggedIn;
        deckBuilderButton.interactable = DBManager.LoggedIn;
        if (DBManager.LoggedIn)
            loggedInText.text = "Welcome: " + DBManager.username;
    }

    IEnumerator _SwitchCanvas(CanvasAlphaControl fromCanvas, CanvasAlphaControl toCanvas)
    {
        fromCanvas.FadeToInvisible();
        yield return new WaitForSeconds(fromCanvas.fadeTime);
        fromCanvas.gameObject.SetActive(false);
        toCanvas.gameObject.SetActive(true);
        toCanvas.FadeToVisible();
    }
    public void GoToMainMenu()
    {
        if (currentMenu != null)
        {
            if (currentMenu != mainMenuCanvas)
                SwitchCanvas(currentMenu, mainMenuCanvas);
            CheckLogin();
        }
    }

    public void GoToRegistration()
    {
        SwitchCanvas(mainMenuCanvas, registerCanvas);
        currentMenu = registerCanvas;
    }

    public void GoToLogin()
    {
        SwitchCanvas(mainMenuCanvas, loginCanvas);
        currentMenu = loginCanvas;
    }
    public void GoToDeckBuilder()
    { 
        SceneManager.LoadScene(2);
    }

    public void GoToGameSingleplayer()
    {
        SceneManager.LoadScene(3);
    }

    public void GoToMultiplayer()
    {
        DBManager.deckIDsMultiplayer = new List<int>();
        DBManager.playMultiplayer = true;
        SceneManager.LoadScene(4);
    }
    public void Logout()
    {
        DBManager.LogOut();
        loggedInText.text = "User not Logged In";
        CheckLogin();
    }

}
