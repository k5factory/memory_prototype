using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ItemCreator : MonoBehaviour
{
    [Header("References")]
    public Button categoryButton;
    public GameObject contentCategoryButtons;
    public GameObject itemPrefab;
    public GameObject contentItemCanvas;

    [Space]
    public List<Item> allItems;

    TextAsset textFile;

    void Start()
    {
        allItems = new List<Item>();


        if (DBManager.LoggedIn)
            StartCoroutine(_GetItemList());
        else
            CreateItems();


    }

    IEnumerator _GetItemList()
    {
        textFile = new TextAsset(File.ReadAllText(DBManager.GetPath("items")));
        yield return new WaitForSeconds(1f);
        CreateItems();
    }


    // Update is called once per frame
    void CreateItems()
    {
        string[] lines = textFile.text.Split("\n"[0]);
        List<string> linesList = new List<string>(lines);


        linesList.RemoveAt(0);
        //Debug.Log("LEERE ELEMENTE ITEM LISTE: " + linesList[linesList.Count - 1]);
        //Debug.Log("LEERE ELEMENTE ITEM LISTE: " + linesList[linesList.Count - 2]);
        linesList.Remove(linesList[linesList.Count - 1]);
        linesList.Remove(linesList[linesList.Count - 1]);

        Debug.Log("LINES COUNT: " + linesList.Count);

        if (DBManager.deckString != "NEWUSERDECK")
            Debug.Log("DB MANAGER DECK INT COUNT: " + DBManager.deckIDsLocal.Count);


        for (int i = 0; i < linesList.Count; i++)
        {
            //if (linesList[i].Length > 2)
            //{
            //Debug.Log(linesList[i]);
            string[] dataValues = linesList[i].Split(',');
            GameObject item = Instantiate(itemPrefab);
            item.transform.SetParent(contentItemCanvas.transform);
            item.transform.localScale = Vector3.one;
            string itemName = dataValues[1];
            item.GetComponent<Item>().nameItem = itemName;
            item.GetComponent<Item>().id = int.Parse(dataValues[0]);

            if (int.Parse(dataValues[2]) == 1)
                item.GetComponent<Item>().mainCategory = Item.MainCategory.Food;
            else if (int.Parse(dataValues[2]) == 2)
                item.GetComponent<Item>().mainCategory = Item.MainCategory.Music;
            else if (int.Parse(dataValues[2]) == 3)
                item.GetComponent<Item>().mainCategory = Item.MainCategory.Love;
            else if (int.Parse(dataValues[2]) == 4)
                item.GetComponent<Item>().mainCategory = Item.MainCategory.Living;
            else if (int.Parse(dataValues[2]) == 5)
                item.GetComponent<Item>().mainCategory = Item.MainCategory.Fashion;

            allItems.Add(item.GetComponent<Item>());

            //}

        }

        DeckCreation.Instance.SetItemList(allItems);
        Debug.Log("ITEM COUNT: " + allItems.Count);
        //Debug.Log("ITEM LAST: " + items[79].nameItem);
        if (DBManager.deckString != "NEWUSERDECK")
        {
            for (int i = 0; i < allItems.Count; i++)
            {
                if (DBManager.deckIDsLocal.Contains(allItems[i].id))
                {
                    allItems[i].GetComponent<Item>().InDeck();
                }
            }
        }

        CreateCategoryButtons();
    }

    private void CreateCategoryButtons()
    {
        Button b = Instantiate(categoryButton).GetComponent<Button>();
        b.gameObject.transform.SetParent(contentCategoryButtons.transform);
        b.gameObject.transform.localScale = Vector3.one;
        b.onClick.AddListener(() => { ShowAllItems(); });
        b.GetComponentInChildren<TextMeshProUGUI>().text = "ALL";
        foreach (Item.MainCategory itemCategory in Enum.GetValues(typeof(Item.MainCategory)))
        {
            Button btn = Instantiate(categoryButton).GetComponent<Button>();
            btn.gameObject.transform.SetParent(contentCategoryButtons.transform);
            btn.gameObject.transform.localScale = Vector3.one;
            btn.onClick.AddListener(() => { SwitchCategory(itemCategory); });
            switch (itemCategory)
            {
                case Item.MainCategory.Food:
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "FOOD";
                    break;
                case Item.MainCategory.Fashion:
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "FASHION";
                    break;
                case Item.MainCategory.Music:
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "MUSIC";
                    break;
                case Item.MainCategory.Living:
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "LIVING";
                    break;
                case Item.MainCategory.Love:
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = "LOVE";
                    break;
            }
               
        }

    }
    private void ShowAllItems()
    {
        foreach (Item i in allItems)
            i.gameObject.SetActive(true);
    }
    private void SwitchCategory(Item.MainCategory category)
    {
        foreach (Item i in allItems)
            i.gameObject.SetActive(false);
        switch (category)
        {
            case Item.MainCategory.Food:               
                foreach (Item i in allItems)
                {
                    if (i.mainCategory == Item.MainCategory.Food)
                        i.gameObject.SetActive(true);
                }
                break;
            case Item.MainCategory.Fashion:
                foreach (Item i in allItems)
                {
                    if (i.mainCategory == Item.MainCategory.Fashion)
                        i.gameObject.SetActive(true);
                }
                break;
            case Item.MainCategory.Music:
                foreach (Item i in allItems)
                {
                    if (i.mainCategory == Item.MainCategory.Music)
                        i.gameObject.SetActive(true);
                }
                break;
            case Item.MainCategory.Living:
                foreach (Item i in allItems)
                {
                    if (i.mainCategory == Item.MainCategory.Living)
                        i.gameObject.SetActive(true);
                }
                break;
            case Item.MainCategory.Love:
                foreach (Item i in allItems)
                {
                    if (i.mainCategory == Item.MainCategory.Love)
                        i.gameObject.SetActive(true);
                }
                break;
        }
    }
}
