using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : ClickBase
{

    public enum MainCategory { Food, Music, Love, Living, Fashion };

    public MainCategory mainCategory;


    [Header("Controls")]
    public bool deckFull = false;

    public bool isCreationPhase = true;

    public Item previousClicked;

    public bool memory = false;
    /*
    public enum Color {Black, White, Red, Green, Blue};

    public Color colorType;*/
    private bool addedToDeck = false;


    public override void Start()
    {
        if (nameText != null && isCreationPhase)
            nameText.text = nameItem;
        else if (nameText != null && !isCreationPhase)
            nameText.text = "X";
    }

    public override void ResetItem()
    {
        base.ResetItem();

        if (nameText != null && !isCreationPhase)
            nameText.text = "X";
        previousClicked = null;
        memory = false;
    }

    private void OnEnable()
    {
        EventManager.ItemClickedDelegate += ItemClicked;
    }

    private void OnDisable()
    {
        EventManager.ItemClickedDelegate -= ItemClicked;
        //RESET
        //gameObject.GetComponent<Image>().color = Color.white;
        //isClicked = false;
    }  

    private void ItemClicked(Item itemElement)
    {
        if (itemElement == this)
        {
            // Already Clicked (Unselect)
            if (isClicked)
            {
                if (isCreationPhase && addedToDeck)
                {
                    DeselectItem();
                    addedToDeck = false;
                    DBManager.FillDeck(id, false);
                    Debug.Log("Remove from Deck: " + itemElement.nameText.text);
                    EventManager.CallRemoveItemFromDeck(itemElement);
                }
            }
            else if (!deckFull)
            {
                SelectItem();

                if (isCreationPhase && !addedToDeck)
                {
                    addedToDeck = true;
                    DBManager.FillDeck(id, true);
                    Debug.Log("Add to Deck: " + itemElement.nameText.text);
                    EventManager.CallAddItemToDeck(itemElement);
                }
                else if (!isCreationPhase)
                {
                    nameText.text = nameItem;

                    if (previousClicked != null)
                    {
                        // Check if same ID
                        if (previousClicked.id == id && previousClicked.isClicked)
                        {
                            CorrectConnection();
                            previousClicked.CorrectConnection();
                            memory = true;
                            previousClicked.memory = true;

                            GameManager.Instance.CorrectSelection();
                        }
                        else if (previousClicked.id != id && previousClicked.isClicked)
                        {
                            WrongConnection();
                            previousClicked.WrongConnection();
                            GameManager.Instance.WrongSelection();
                        }
                    }
                }
            }
        }
        else
        {
            // PLAY PHASE, if same ID, same Element
            if (!isCreationPhase)
                previousClicked = itemElement;
        }
    }
    public void WrongConnection()
    {
        StartCoroutine(_WrongConnection());
    }

    public void CorrectConnection()
    {
        StartCoroutine(_CorrectConnection());
    }

    public void FailedMemory()
    {
        StartCoroutine(_FailedMemory());
    }
    IEnumerator _CorrectConnection()
    {
       
        yield return new WaitForSeconds(0.2f);
        gameObject.GetComponent<Image>().color = Color.green;
        yield return new WaitForSeconds(1f);
        gameObject.GetComponent<Image>().color = Color.white;
        isClicked = false;
        canBeClicked = true;
        gameObject.SetActive(false);
    }
    IEnumerator _WrongConnection()
    {
        yield return new WaitForSeconds(0.2f);
        canBeClicked = false;
        gameObject.GetComponent<Image>().color = Color.red;
        yield return new WaitForSeconds(1f);
        nameText.text = "X";
        canBeClicked = true;
        isClicked = false;
        gameObject.GetComponent<Image>().color = Color.white;
    }

    IEnumerator _FailedMemory()
    {
        nameText.text = "X";
        yield return new WaitForSeconds(0.2f);
        canBeClicked = false;
        gameObject.GetComponent<Image>().color = Color.red;
        yield return new WaitForSeconds(1f);
        gameObject.GetComponent<Image>().color = Color.white;
        isClicked = false;
        canBeClicked = true;
        gameObject.SetActive(false);
    }

    public void InDeck()
    {
        SelectItem();
        addedToDeck = true;
        Debug.Log("Add to Deck: " + nameItem);
        EventManager.CallAddItemToDeck(this);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        if (gameObject.activeSelf && canBeClicked)
        {
            EventManager.OnItemClick(this);
        }
    }
}