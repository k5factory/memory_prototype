﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasAlphaControl : MonoBehaviour {

    public bool autoFadeIn = false;
    public bool autoFadeOut = false;
    public bool fadeOutAfterTime;
    public float fadeTime = 1f;
    public bool showButtons = false;

    float multiplier;
    public CanvasGroup cGroup;
    public float fadeTimerAutomatic = 1f;

    public UnityEvent fadeToInvisibleFinished;
    public UnityEvent fadeToVisibleFinished;
    public UnityEvent fadeToInvisibleWaitFinished;
    
    void Awake()
    {
        cGroup = GetComponent<CanvasGroup>();
        if (autoFadeIn)
            autoFadeOut = false;

        if (autoFadeOut)
        {
            cGroup.alpha = 1f;
            StartCoroutine(_FadeToInvisible());
        }
        else if (autoFadeIn)
        {
            cGroup.alpha = 0f;
            StartCoroutine(_FadeToVisible());
        }

        if (fadeOutAfterTime)
        {
            StartCoroutine(_autoFadeAfterTime());
        }
    }
    void Update()
    {
        if (autoFadeIn)
            autoFadeOut = false;

        if (autoFadeOut)
        {
            cGroup.alpha = 1f;
            StartCoroutine(_FadeToInvisible());
        }
        else if (autoFadeIn)
        {
            cGroup.alpha = 0f;
            StartCoroutine(_FadeToVisible());
        }
    }
    public void FadeToInvisibleAfterTime(float time)
    {
        StartCoroutine(_FadeToInvisibleAfterTime(time));
    }

    public IEnumerator _FadeToInvisibleAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        multiplier = 1f / fadeTime;
        float alpha = cGroup.alpha;
        while (alpha > 0)
        {
            alpha -= Time.deltaTime * multiplier;
            cGroup.alpha = alpha;
            yield return new WaitForEndOfFrame();
        }
        fadeToInvisibleWaitFinished.Invoke();
    }

    public void FadeToInvisible()
    {
        StartCoroutine(_FadeToInvisible());
    }

    public void FadeToVisible()
    {
        StartCoroutine(_FadeToVisible());
    }
    IEnumerator _autoFadeAfterTime()
    {
        yield return new WaitForSeconds(fadeTimerAutomatic);
        StartCoroutine(_FadeToInvisible());
    }


    public IEnumerator _FadeToInvisible()
    {
        multiplier = 1f / fadeTime;
        float alpha = cGroup.alpha;
        while (alpha > 0)
        {
            alpha -= Time.deltaTime * multiplier;
            cGroup.alpha = alpha;
            yield return new WaitForEndOfFrame();
        }
        fadeToInvisibleFinished.Invoke();
    }

    public IEnumerator _FadeToVisible()
    {
        multiplier = 1f / fadeTime;
        float alpha = cGroup.alpha;
        while (alpha < 1)
        {
            alpha += Time.deltaTime * multiplier;
            cGroup.alpha = alpha;
            yield return new WaitForEndOfFrame();
        }
        fadeToVisibleFinished.Invoke();
    }

    void OnGUI()
    {
        if (!showButtons)
            return;

        if (GUI.Button(new Rect(50, 350, 100, 45), "AlphaCTRL \n fromBLACK"))
            FadeToInvisible();
        if (GUI.Button(new Rect(50, 400, 100, 45), "AlphaCTRL \n toBLACK"))
            FadeToVisible();
    }                
}
