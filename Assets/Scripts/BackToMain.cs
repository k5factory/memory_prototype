using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMain : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    public void CallBackToMain()
    {
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByBuildIndex(1))
        {
            DBManager.playMultiplayer = false;
            SceneManager.LoadScene(1);
        }
        else
        {
            MainMenu.Instance.GoToMainMenu();
        }
    }
}
