using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Registration : MonoBehaviour
{
    public Login login;

    public TMP_InputField nameField;
    public TMP_InputField passwordField;
    public TextMeshProUGUI failureText;
    public Button submitButton;

    Coroutine Failure;
    public void CallRegister()
    {
        StartCoroutine(_CallRegistration());
    }

    IEnumerator _CallRegistration()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", nameField.text);
        form.AddField("password", passwordField.text);

        UnityWebRequest request = UnityWebRequest.Post(DBManager.url+"register.php", form);
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (request.downloadHandler.text == "")
            {
                PlayerPrefs.SetString("username", nameField.text);
                PlayerPrefs.SetString("password", passwordField.text);
                Debug.Log("User created successfully");
                Failure = StartCoroutine(_Failure("User created successfully"));
                StartCoroutine(_LoginAutomatically());

            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                StartCoroutine(_Failure(request.downloadHandler.text));
            }
        }
    }
    IEnumerator _LoginAutomatically()
    {
        yield return new WaitForSeconds(1f);
        if (Failure != null)
            StopCoroutine(Failure);
        Failure = StartCoroutine(_Failure("Logging In..."));
        login.CallLoginUsername(nameField.text, passwordField.text);
    }

    IEnumerator _Failure(string failure)
    {
        failureText.gameObject.SetActive(true);
        failureText.text = failure;
        yield return new WaitForSeconds(3f);
        failureText.gameObject.SetActive(false);
    }
    public void VerifyInput()
    {
        submitButton.interactable = (nameField.text.Length >= 3 && passwordField.text.Length >= 3);
    }
}
