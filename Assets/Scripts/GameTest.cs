using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameTest : MonoBehaviour
{
    public TextMeshProUGUI playerDisplay;
    public TextMeshProUGUI scoreDisplay;
    

    private void Awake()
    {
        if (!DBManager.LoggedIn)
            SceneManager.LoadScene(0);

        playerDisplay.text = "Player: " + DBManager.username;
       // scoreDisplay.text = "Score: " + DBManager.score;
    }

    public void CallSaveData()
    {
        StartCoroutine(_SavePlayerData());
    }

    IEnumerator _SavePlayerData()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", DBManager.username);
        //form.AddField("score", DBManager.score);

        UnityWebRequest request = UnityWebRequest.Post(DBManager.url + "savedata.php", form);
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (request.downloadHandler.text[0] == '0')
            {
                Debug.Log("Game Saved");
            }
            else
                Debug.Log(request.downloadHandler.text);
        }

        DBManager.LogOut();
        SceneManager.LoadScene(0);
    }

    public void IncreaseScore()
    {
        //DBManager.score++;
        //scoreDisplay.text = DBManager.score.ToString();
    }
}
