using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Players : ClickBase, IPointerClickHandler
{
    private string playerDeckString;
    public int similarTaste = 0;

    private void OnEnable()
    {
        EventManager.PlayerClickedDelegate += PlayerClicked;
    }
    private void OnDisable()
    {
        EventManager.PlayerClickedDelegate -= PlayerClicked;
    }

    public void SetPlayerDeck(string playerDeck)
    {
        playerDeckString = playerDeck;
        Debug.Log("Other Player Deck: " + playerDeckString + " LENGTH: "+ playerDeckString.Split(';').Length);
    }
    public string GetPlayerDeck()
    {
        return playerDeckString;
    }
    private void PlayerClicked(Players player)
    {
        if (player == this)
        {
            if (isClicked)
            {
                base.DeselectItem();
            }

            else
            {
                base.SelectItem();
            }
        }
        else
        {
            base.DeselectItem();
        }

    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        if (gameObject.activeSelf && canBeClicked)
        {
            EventManager.OnPlayerClick(this);
        }
    }
}
