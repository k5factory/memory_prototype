using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeckCreation : MonoBehaviour
{
    public static DeckCreation Instance;
    [Header("References")]
    public Button saveDeckButton;
    List<Item> AllItemList = new List<Item>();

    [Header("Controls")]
    public int maxDeckSize = 10;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }
    private void Start()
    {
        DBManager.deckItems = new List<Item>();
        saveDeckButton.gameObject.SetActive(false);

    }
    private void OnEnable()
    {
        //EventManager.AddItemsDelegate += AddItemsToDeck;
        EventManager.AddItemToDeckDelegate += AddedItemToDeck;
        EventManager.RemoveItemFromDeckDelegate += RemoveItemFromDeck;
    }

    private void OnDisable()
    {
        //EventManager.AddItemsDelegate -= AddItemsToDeck;
        EventManager.AddItemToDeckDelegate -= AddedItemToDeck;
        EventManager.RemoveItemFromDeckDelegate -= RemoveItemFromDeck;
    }


    public void SetItemList(List<Item> ItemList)
    {
        AllItemList = ItemList;
        //StartDeckCreationPairs(null, null);
    }
    private void AddedItemToDeck(Item item)
    {
        DBManager.deckItems.Add(item);
        if (DBManager.deckItems.Count >= maxDeckSize)
        {
            Debug.Log("Deck Count: " + DBManager.deckItems.Count);
            Debug.Log("DECK FULL PLAY BUTTON");
            saveDeckButton.gameObject.SetActive(true);

            foreach (Item i in AllItemList)
                i.deckFull = true;
        }


    }
    private void RemoveItemFromDeck(Item item)
    {
        DBManager.deckItems.Remove(item);
        if (DBManager.deckItems.Count < maxDeckSize)
        {
            Debug.Log("Deck Count Remove: " + DBManager.deckItems.Count);
            saveDeckButton.gameObject.SetActive(false);
            foreach (Item i in AllItemList)
                i.deckFull = false;
        }

    }
    public void SaveDeckFinish()
    {
        SceneManager.LoadScene(3);
    }


    /*
    private void AddItemsToDeck(Item itemOne, Item itemTwo)
    {
        DeckItems.Add(itemOne);
        DeckItems.Add(itemTwo);

        if (DeckItems.Count < deckSize)
        {
            GameManager.Instance.StartDeckCreationPairs(itemOne, itemTwo);
        }
        else
        {
            StartCoroutine(_PlayDeck());
        }
    }

    IEnumerator _PlayDeck()
    {
        yield return new WaitForSeconds(1f);
        GameManager.Instance.PlayDeck(DeckItems);
    }*/
}
