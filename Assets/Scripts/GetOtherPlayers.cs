using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GetOtherPlayers : MonoBehaviour
{
    // Start is called before the first frame update

    [Header("References")]
    public Button playDeckButton;
    public GameObject playerPrefab;
    public TextAsset playersFile;
    public GameObject contentCanvas;

    public List<Players> otherPlayers = new List<Players>();


    private void OnEnable()
    {
        EventManager.PlayerClickedDelegate += PlayerSelected;
    }

    private void OnDisable()
    {
        EventManager.PlayerClickedDelegate -= PlayerSelected;
    }
    void Start()
    {
        playDeckButton.gameObject.SetActive(false);
        FindOtherPlayers();
    }
    private void PlayerSelected(Players player)
    {
        if (!player.isClicked)
        {
            playDeckButton.gameObject.SetActive(true);
            DBManager.FillDeckMultiplayer(player);
            Debug.Log("SET PLAYER: " + player.nameItem);
        }
        else
        {
            playDeckButton.gameObject.SetActive(false);
            Debug.Log("SET PLAYER NULL");
        }
    }

    public void FindOtherPlayers()
    {
        StartCoroutine(_GetOtherPlayers());
    }

    public void PlayDeckOfPlayer()
    {
        SceneManager.LoadScene(3);
    }

    IEnumerator _GetOtherPlayers()
    {
        playersFile = new TextAsset(File.ReadAllText(DBManager.GetPath("players")));

        yield return new WaitForSeconds(1f);
        CreatePlayers();
    }

    public void SortByTaste()
    {
        List<Players> sortedListByTaste = new List<Players>();
        string[] stringSplitLocal = DBManager.deckString.Split(';');
        if (stringSplitLocal.Length > 2)
        {
            for (int i = 0; i < otherPlayers.Count; i++)
            {
                otherPlayers[i].similarTaste = 0;
                string[] stringSplitOther = otherPlayers[i].GetPlayerDeck().Split(';');

                for (int j = 0; j < stringSplitOther.Length; j++)
                {
                    if (stringSplitOther[j] == "1")
                    {
                        if (stringSplitOther[j] == stringSplitLocal[j])
                            otherPlayers[i].similarTaste++;
                    }
                }
                sortedListByTaste.Add(otherPlayers[i]);
            }
        }

        sortedListByTaste.Sort((p1, p2) => p1.similarTaste.CompareTo(p2.similarTaste));

        foreach (Players p in sortedListByTaste)
        {
            p.gameObject.transform.SetParent(null);
        }
        for(int i = sortedListByTaste.Count-1; i >= 0; i--)
        {
            sortedListByTaste[i].transform.SetParent(contentCanvas.transform);
            sortedListByTaste[i].transform.localScale = Vector3.one;

        }
    }
    private void CreatePlayers()
    {
        string[] lines = playersFile.text.Split("\n"[0]);
        Debug.Log("LINES COUNT: " + lines.Length);

        for (int i = 1; i < lines.Length; i++)
        {
            if (lines[i].Length > 2)
            {
                Debug.Log(lines[i]);

                string[] dataValues = lines[i].Split(',');
                GameObject player = Instantiate(playerPrefab);
                player.transform.SetParent(contentCanvas.transform);
                player.transform.localScale = Vector3.one;
                string playerName = dataValues[1];
                player.GetComponent<Players>().nameItem = playerName;
                player.GetComponent<Players>().id = int.Parse(dataValues[0]);

                player.GetComponent<Players>().SetPlayerDeck(dataValues[2]);


                otherPlayers.Add(player.GetComponent<Players>());
            }
        }
    }
}
