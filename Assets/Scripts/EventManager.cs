using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager
{

    public delegate void CallItemClicked(Item item);
    public static CallItemClicked ItemClickedDelegate;

    public static void OnItemClick(Item item)
    {
        ItemClickedDelegate(item);
    }

    public delegate void CallMemoryClicked(ItemMemory item);
    public static CallMemoryClicked MemoryClickDelegate;

    public static void OnMemoryClick(ItemMemory item)
    {
        MemoryClickDelegate(item);
    }

    public delegate void AddItemsToDeck(Item itemOne, Item itemTwo);
    public static AddItemsToDeck AddItemsDelegate;

    public static void CallAddToDeck(Item itemOne, Item itemTwo)
    {
        AddItemsDelegate(itemOne,itemTwo);
    }

    public delegate void AddItemToDeck(Item item);
    public static AddItemToDeck AddItemToDeckDelegate;

    public static void CallAddItemToDeck(Item item)
    {
        AddItemToDeckDelegate(item);
    }

    public delegate void RemoveItemFromDeck(Item item);
    public static RemoveItemFromDeck RemoveItemFromDeckDelegate;

    public static void CallRemoveItemFromDeck(Item item)
    {
        RemoveItemFromDeckDelegate(item);
    }

    public delegate void CallPlayerClicked(Players player);
    public static CallPlayerClicked PlayerClickedDelegate;

    public static void OnPlayerClick(Players player)
    {
        PlayerClickedDelegate(player);
    }
}
