using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    public WelcomeScreen welcomeScreen;
    public TMP_InputField nameField;
    public TMP_InputField passwordField;
    public TextMeshProUGUI failureText;
    public Button loginButton;

    private bool firstLogin = true;

    private void Start()
    {
        if (failureText != null)
            failureText.gameObject.SetActive(false);
    }
    public void CallLoginNew()
    {
        StartCoroutine(_CallLoginNew(null, null));
    }

    public void CallLoginUsername(string username, string password)
    {
        firstLogin = false;
        StartCoroutine(_CallLoginNew(username, password));
    }

    IEnumerator _CallLoginNew(string username, string password)
    {
        WWWForm form = new WWWForm();

        if (username == null && password == null)
        {
            form.AddField("name", nameField.text);
            form.AddField("password", passwordField.text);
        }
        else
        {
            form.AddField("name", username);
            form.AddField("password", password);
        }

        UnityWebRequest request = UnityWebRequest.Post(DBManager.url + "login.php", form);
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (request.downloadHandler.text[0] == '0')
            {
                Debug.Log("Login Success");

                if (request.downloadHandler.text.Split('\t')[1] != "NEWUSERDECK")
                    DBManager.GetDeckDB(request.downloadHandler.text.Split('\t')[1]);
                else
                {
                    DBManager.deckString = "NEWUSERDECK";
                }

                if (firstLogin)
                {
                    DBManager.username = nameField.text;
                    PlayerPrefs.SetString("username", DBManager.username);
                    PlayerPrefs.SetString("password", passwordField.text);

                    if (MainMenu.Instance != null)
                        MainMenu.Instance.GoToMainMenu();

                    DownloadLists.Instance.DownloadPlayers();
                    DownloadLists.Instance.DownloadItems();
                }
                else if (welcomeScreen != null)
                {
                    DBManager.username = username;
                    welcomeScreen.CallWelcome();
                }
                else if (MainMenu.Instance != null)
                {
                    DBManager.username = username;
                    MainMenu.Instance.GoToMainMenu();

                    DownloadLists.Instance.DownloadPlayers();
                    DownloadLists.Instance.DownloadItems();
                }
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                if(welcomeScreen!= null)
                    welcomeScreen.CallWelcome();
                if (failureText != null)
                    StartCoroutine(_Failure(request.downloadHandler.text));
            }
        }
    }

    IEnumerator _Failure(string failure)
    {
        failureText.gameObject.SetActive(true);
        failureText.text = failure;
        yield return new WaitForSeconds(3f);
        failureText.gameObject.SetActive(false);
    }
    public void VerifyInputs()
    {
        loginButton.interactable = (nameField.text.Length >= 3 && passwordField.text.Length >= 3);
    }
}
