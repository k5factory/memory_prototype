using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBManager
{


    public static string url = "http://vrtravel.synology.me/phpMyAdmin/sqlconnect/";


    public static string username;

    public static List<Item> allItems = new List<Item>();



    public static List<int> deckIDsLocal = new List<int>();

    public static string deckString;
    public static List<Item> deckItems = new List<Item>();
    public static bool LoggedIn { get { return username != null; } }


    public static List<int> deckIDsMultiplayer = new List<int>();
    public static bool playMultiplayer = false;
    public static void LogOut()
    {
        username = null;
        playMultiplayer = false;
        deckItems = new List<Item>();
        deckIDsLocal = new List<int>();
        deckIDsMultiplayer = new List<int>();
    }



    public static void FillDeckMultiplayer(Players otherPlayer)
    {
        if (otherPlayer != null)
        {
            string[] deckPlayerSplit = otherPlayer.GetPlayerDeck().Split(';');
            //Debug.Log("DECK SPLIT: " + deckPlayerSplit[0]);            
            for (int i = 0; i < deckPlayerSplit.Length; i++)
            {
                int result = 0;
                int.TryParse(deckPlayerSplit[i], out result);
                deckIDsMultiplayer.Add(result);
                //Debug.Log("Deck ELEMENT: " + deckInt[i]);
            }

        }
        else
            Debug.Log("NO PLAYER SELECTED");
    }


    public static void FillDeck(int id, bool fill)
    {
        //Debug.Log("DECK_INT LENGTH: " + deckIDsLocal.Count);
        if (fill)
            deckIDsLocal.Add(id);
        else
            deckIDsLocal.Remove(id);
     
        deckString = "";

        deckString = string.Join(";", deckIDsLocal);

        Debug.Log("FILLED DECK STRING: " + deckString + " LENGTH: " + deckString.Split(';').Length);

    }

    public static void GetDeckDB(string deckDB)
    {
        deckString = deckDB;
        string[] deckDBSplit = deckDB.Split(';');
        Debug.Log("DECK STRING: " + deckString);
        List<string> listDeckDBSplit = new List<string>(deckDBSplit);


        for (int i = 0; i < listDeckDBSplit.Count; i++)
        {
            int result = 0;
            int.TryParse(listDeckDBSplit[i], out result);
            //Debug.Log("Deck ELEMENT: " + deckInt[i]);
            deckIDsLocal.Add(result);
        }
        
    }


    // Following method is used to retrive the relative path as device platform
    public static string GetPath(string fileName)
    {
#if UNITY_EDITOR
        return Application.dataPath + "/CSV/" + fileName + ".csv";
#elif UNITY_ANDROID
        return Application.persistentDataPath+ fileName + ".csv";
#elif UNITY_IPHONE
        return Application.persistentDataPath+ fileName + ".csv";
#else
        return Application.dataPath +"/"+ fileName + ".csv";
#endif
    }

}
