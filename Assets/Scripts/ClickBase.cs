using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickBase : MonoBehaviour, IPointerClickHandler
{
    [Header("Settings")]
    public int id;
    public string nameItem;

    public TextMeshProUGUI nameText;
    public Sprite icon;

    [Header("Controls")]
    public bool canBeClicked = true;
    public bool isClicked = false;


    public virtual void Start()
    {
        if (nameText != null)
            nameText.text = nameItem;
    }

    public virtual void ResetItem()
    {
        canBeClicked = true;

        DeselectItem();
    }

    public virtual void SelectItem()
    {
        GetComponent<Image>().color = Color.green;
        isClicked = true;
    }

    public virtual void DeselectItem()
    {
        GetComponent<Image>().color = Color.white;
        isClicked = false;
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("ITEM CLICKED: " + nameItem);
    }
}
