using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameManager Instance;
    [Header("References")]
    public Button playAgainButton;
    public TextMeshProUGUI wonFailText;
    public TextMeshProUGUI failureCountText;
    public GameObject contentParent;
    public GameObject memoryPrefab;
    [Header("Controls")]
    public int failureCount = 10;

    int currentFails = 0;
    List<Item> currentMemoryList = new List<Item>();


    TextAsset itemFile;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);


    }

    private void Start()
    {
        failureCountText.text = currentFails.ToString();
        playAgainButton.gameObject.SetActive(false);
        wonFailText.gameObject.SetActive(false);

        if (DBManager.playMultiplayer)
            StartCoroutine(_CreateMemory(DBManager.deckIDsMultiplayer));
        else
            StartCoroutine(_CreateMemory(DBManager.deckIDsLocal));
    }


    public void RestartMemory()
    {
        foreach (Item i in currentMemoryList)
        {
            i.gameObject.SetActive(false);
            i.gameObject.transform.SetParent(null);
        }

        contentParent.GetComponent<GridLayoutGroup>().enabled = true;

        currentFails = 0;
        failureCountText.text = currentFails.ToString();
        wonFailText.gameObject.SetActive(false);
        playAgainButton.gameObject.SetActive(false);

        currentMemoryList = ShuffleItemList(currentMemoryList);
        foreach (Item i in currentMemoryList)
        {
            i.gameObject.transform.SetParent(contentParent.transform);
            i.gameObject.transform.localScale = Vector3.one;
            i.ResetItem();
            i.gameObject.SetActive(true);
        }

        Invoke(nameof(DisableGridLayout), 1f);


    }
    IEnumerator _CreateMemory(List<int> whichListToCheck)
    {

        itemFile = new TextAsset(File.ReadAllText(DBManager.GetPath("items")));
        yield return new WaitForSeconds(1f);
        CreateMemoryItems(whichListToCheck);
    }

    private void CreateMemoryItems(List<int> whichListToCheck)
    {
        string[] lines = itemFile.text.Split("\n"[0]);
        List<string> linesList = new List<string>(lines);

        // Lines are empty at that point
        linesList.Remove(linesList[0]);
        linesList.Remove(linesList[linesList.Count - 1]);
        linesList.Remove(linesList[linesList.Count - 1]);

        Debug.Log("LINES COUNT: " + linesList.Count);

        for (int i = 0; i < linesList.Count; i++)
        {

            // If Player has Item with ID in his Deck
            string[] dataValues = linesList[i].Split(',');
            int currentItemID = int.Parse(dataValues[0]);

            if (whichListToCheck.Contains(currentItemID))
            {
                for (int j = 0; j < 2; j++)
                {
                    Debug.Log(" CURRENT ITEM" + linesList[i]);

                    Item item = Instantiate(memoryPrefab).GetComponent<Item>();
                    item.transform.localScale = Vector3.one;
                    string itemName = dataValues[1];
                    item.nameItem = itemName;
                    item.id = currentItemID;
                    item.isCreationPhase = false;

                    // Set Category CURRENTLY not necessary
                    if (int.Parse(dataValues[2]) == 1)
                        item.GetComponent<Item>().mainCategory = Item.MainCategory.Food;
                    else if (int.Parse(dataValues[2]) == 2)
                        item.GetComponent<Item>().mainCategory = Item.MainCategory.Music;
                    else if (int.Parse(dataValues[2]) == 3)
                        item.GetComponent<Item>().mainCategory = Item.MainCategory.Love;
                    else if (int.Parse(dataValues[2]) == 4)
                        item.GetComponent<Item>().mainCategory = Item.MainCategory.Living;
                    else if (int.Parse(dataValues[2]) == 5)
                        item.GetComponent<Item>().mainCategory = Item.MainCategory.Fashion;


                    currentMemoryList.Add(item);
                    item.gameObject.SetActive(false);
                }
            }
        }
        currentMemoryList = ShuffleItemList(currentMemoryList);
        foreach (Item i in currentMemoryList)
        {
            i.gameObject.transform.SetParent(contentParent.transform);
            i.gameObject.transform.localScale = Vector3.one;
            i.gameObject.SetActive(true);
        }


        Invoke(nameof(DisableGridLayout), 1f);
    }

    public void DisableGridLayout()
    {
        contentParent.GetComponent<GridLayoutGroup>().enabled = false;
    }
    public List<Item> ShuffleItemList(List<Item> aList)
    {

        System.Random _random = new System.Random();

        Item myGO;

        int n = aList.Count;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(_random.NextDouble() * (n - i));
            myGO = aList[r];
            aList[r] = aList[i];
            aList[i] = myGO;
        }

        return aList;
    }

    public void CorrectSelection()
    {
        StartCoroutine(_WaitForNextSelection());


        bool checkWon = false;
        for (int i = 0; i < currentMemoryList.Count; i++)
        {
            if (!currentMemoryList[i].memory)
            {
                checkWon = false;
                return;
            }
            else
                checkWon = true;

        }

        if (checkWon)
        {
            Debug.Log("PLAYER WON");
            wonFailText.text = "YOU WON";
            wonFailText.gameObject.SetActive(true);
            playAgainButton.gameObject.SetActive(true);
        }

    }

    public void WrongSelection()
    {
        currentFails++;
        failureCountText.text = currentFails.ToString();
        StartCoroutine(_WaitForNextSelection());

        if (currentFails == failureCount)
        {
            StartCoroutine(_LostMemory());
        }
    }

    IEnumerator _LostMemory()
    {
        yield return new WaitForSeconds(1f);
        foreach (Item i in currentMemoryList)
        {
            if (i.gameObject.activeSelf)
                i.FailedMemory();
        }
        playAgainButton.gameObject.SetActive(true);
        wonFailText.text = "YOU LOST";
        wonFailText.gameObject.SetActive(true);
    }

    IEnumerator _WaitForNextSelection()
    {
        foreach (Item i in currentMemoryList)
        {
            i.canBeClicked = false;
        }
        yield return new WaitForSeconds(1f);
        foreach (Item i in currentMemoryList)
        {
            i.canBeClicked = true;
            i.previousClicked = null;
        }
    }
}
