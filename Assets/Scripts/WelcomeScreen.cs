using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WelcomeScreen : MonoBehaviour
{
    public bool deletePlayerPrefs = false;
    public Login login;
    public TextMeshProUGUI usernameDisplay;

    private void Start()
    {
        if (deletePlayerPrefs)
            PlayerPrefs.DeleteAll();

        usernameDisplay.gameObject.SetActive(false);
        CheckNewPlayer();

    }

    void CheckNewPlayer()
    {
        if (PlayerPrefs.HasKey("username") && !DBManager.LoggedIn)
        {
            login.CallLoginUsername(PlayerPrefs.GetString("username"), PlayerPrefs.GetString("password"));

        }
        else
        { 
            CallWelcome();
        }

    }

    public void CallWelcome()
    {
        if(DBManager.LoggedIn)
            usernameDisplay.text = DBManager.username;
        else
            usernameDisplay.text = "NEW PLAYER";
        StartCoroutine(_Welcome());
    }

    IEnumerator _Welcome()
    {
        usernameDisplay.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(1);

    }

}
