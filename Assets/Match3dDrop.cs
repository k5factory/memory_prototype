using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Match3dDrop : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform[] placePositions;
    public DragRigidbodies dragClass;
    private Match3DItem firstInTrigger;
    private Match3DItem secondInTrigger;

    private bool checkTrigger = true;
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GetComponent<Match3DItem>() != null && checkTrigger)
        {
            checkTrigger = false;
            if (firstInTrigger == null)
            {

                firstInTrigger = other.gameObject.GetComponent<Match3DItem>();
                firstInTrigger.inDropZone = true;
                firstInTrigger.GetComponent<Rigidbody>().isKinematic = true;
                LeanTween.move(firstInTrigger.gameObject, placePositions[0], 1f).setEaseOutBounce();
                LeanTween.rotate(firstInTrigger.gameObject, placePositions[0].transform.localEulerAngles, 1f)
                    .setEaseOutBounce();
            }
            else if (secondInTrigger == null && firstInTrigger.inDropZone)
            {

                secondInTrigger = other.gameObject.GetComponent<Match3DItem>();
                secondInTrigger.inDropZone = true;
                secondInTrigger.GetComponent<Rigidbody>().isKinematic = true;
                LeanTween.move(secondInTrigger.gameObject, placePositions[1], 1f).setEaseOutBounce();
                LeanTween.rotate(secondInTrigger.gameObject, placePositions[1].transform.localEulerAngles, 1f)
                    .setEaseOutBounce();

                if (CheckMatch())
                {
                    Debug.Log("CORRECT MATCH");
                    Match3DPrototype.Instance.CorrectMatch(firstInTrigger, secondInTrigger);
                }
                else
                {
                    Match3DPrototype.Instance.IncorrectMatch();
                    LeanTween.cancel(secondInTrigger.gameObject);
                    Debug.Log("INCORRECT MATCH");
                    secondInTrigger.GetComponent<Rigidbody>().isKinematic = false;
                    secondInTrigger.GetComponent<Rigidbody>().AddForce(0f, 0f, 500f);
                    secondInTrigger.inDropZone = false;
                    secondInTrigger = null;
                }
            }
            //dragClass.ReleaseObject();

            Invoke(nameof(CheckTriggerAgain), 1f);
        }
    }

    private void CheckTriggerAgain()
    {
        checkTrigger = true;
    }

    /*
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Match3DItem>() != null)
        {
            if (other.gameObject.GetComponent<Match3DItem>() == firstInTrigger)
            {
                firstInTrigger.inDropZone = false;
                firstInTrigger = null;
            }

            if (other.gameObject.GetComponent<Match3DItem>() == secondInTrigger)
            {
                secondInTrigger.inDropZone = false;
                secondInTrigger = null;
            }
        }
    }*/

    private bool CheckMatch()
    {
        if (firstInTrigger.id == secondInTrigger.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void DisableColliderShort()
    {
        StartCoroutine(_DisableCollider());
    }

    IEnumerator _DisableCollider()
    {
        if (firstInTrigger)
        {
            firstInTrigger.inDropZone = false;
            firstInTrigger = null;
        }

        if (secondInTrigger)
        {
            secondInTrigger.inDropZone = false;
            secondInTrigger = null;
        }
        GetComponent<CapsuleCollider>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        GetComponent<CapsuleCollider>().enabled = true;
    }

    public void Clear()
    {
        firstInTrigger = null;
        secondInTrigger = null;
    }
}
