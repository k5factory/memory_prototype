using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Match3DPrototype : MonoBehaviour
{
    // Start is called before the first frame update
    public Match3dDrop dropZone;

    public GameObject[] spawnPoints;
    public string itemListName = "itemsTest";
    public int countOfMemoryItems = 12;
    public static Match3DPrototype Instance;
    [Header("References")]
    public Button playAgainButton;

    public CanvasAlphaControl[] winScreens;
    public TextMeshProUGUI failureCountText;
    public Slider remainingTriesSlider;
    [Space]
    public TextMeshProUGUI timeCountText;
    public TextMeshProUGUI pointCountText;


    public GameObject memoryPrefab;

    [Header("Controls")]
    public int failureCount = 10;

    List<Match3DItem> currentItemList = new List<Match3DItem>();


    TextAsset itemFile;

    private bool startTimer = false;
    private float currentTime = 0;

    private int currentIndexWinScreen = 0;

    private bool checkWinOnce = true;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);


    }

    private void Start()
    {
        remainingTriesSlider.value = 0;
        remainingTriesSlider.maxValue = failureCount;
        failureCountText.text = failureCount.ToString();
        playAgainButton.gameObject.SetActive(false);

        foreach (CanvasAlphaControl c in winScreens)
        {
            c.cGroup.alpha = 0f;
            c.gameObject.SetActive(false);
        }

        StartCoroutine(_CreateMemory());

    }

    private void Update()
    {
        if (startTimer)
        {
            currentTime += Time.deltaTime;
            float minutes = Mathf.FloorToInt(currentTime / 60);
            float seconds = Mathf.FloorToInt(currentTime % 60);
            timeCountText.text = minutes.ToString(("0")) + ":" + seconds.ToString(("00"));

        }
    }


    public void RestartMemory()
    {
        checkWinOnce = true;
        pointCountText.text = "0";
        startTimer = false;
        currentTime = 0;
        timeCountText.text = "0:00";
        foreach (Match3DItem i in currentItemList)
        {
            i.gameObject.SetActive(false);
        }
        remainingTriesSlider.value = 0;
        failureCountText.text = failureCount.ToString();


        foreach (CanvasAlphaControl c in winScreens)
        {
            c.cGroup.alpha = 0f;
            c.gameObject.SetActive(false);
        }

        playAgainButton.gameObject.SetActive(false);

        StartCoroutine(_EnableMemory());


    }

    IEnumerator _EnableMemory()
    {

        for (int i = 0; i < currentItemList.Count; i++)
        {
            int randSpawn = (int)Random.Range(0, spawnPoints.Length);
            currentItemList[i].transform.position = spawnPoints[randSpawn].transform.position;
            currentItemList[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);

        }

        foreach (Match3DItem i in currentItemList)
        {
            i.canBeClicked = true;
        }

        startTimer = true;
        yield return new WaitForSeconds(0.1f);


    }
    IEnumerator _CreateMemory()
    {
        itemFile = Resources.Load(itemListName) as TextAsset;

        yield return new WaitForSeconds(0.5f);
        string[] lines = itemFile.text.Split("\n"[0]);
        List<string> linesList = new List<string>(lines);

        // Lines are empty at that point
        linesList.Remove(linesList[0]);
        linesList.Remove(linesList[linesList.Count - 1]);
        //linesList.Remove(linesList[linesList.Count - 1]);

        Debug.Log("LINES COUNT: " + linesList.Count);

        List<string> pickedItemList = new List<string>();
        List<int> randList = new List<int>();
        int rand = 0;
        for (int i = 0; i < countOfMemoryItems; i++)
        {
            do
            {
                rand = Random.Range(0, linesList.Count);

            } while (randList.Contains(rand));

            randList.Add(rand);
        }
        for (int i = 0; i < randList.Count; i++)
        {
            pickedItemList.Add(linesList[randList[i]]);
        }


        for (int i = 0; i < pickedItemList.Count; i++)
        {

            // If Player has Item with ID in his Deck
            string[] dataValues = pickedItemList[i].Split(',');
            int currentItemID = int.Parse(dataValues[0]);

            for (int j = 0; j < 2; j++)
            {
                Debug.Log(" CURRENT ITEM" + pickedItemList[i]);

                Match3DItem item = Instantiate(memoryPrefab).GetComponent<Match3DItem>();

                string itemName = dataValues[1];
                item.nameItem = itemName;
                item.id = currentItemID;

                // Set Category CURRENTLY not necessary
                if (int.Parse(dataValues[2]) == 1)
                    item.mainCategory = Match3DItem.MainCategory.Food;
                else if (int.Parse(dataValues[2]) == 2)
                    item.mainCategory = Match3DItem.MainCategory.Music;
                else if (int.Parse(dataValues[2]) == 3)
                    item.mainCategory = Match3DItem.MainCategory.Love;
                else if (int.Parse(dataValues[2]) == 4)
                    item.mainCategory = Match3DItem.MainCategory.Living;
                else if (int.Parse(dataValues[2]) == 5)
                    item.mainCategory = Match3DItem.MainCategory.Fashion;

                var spriteMain = Resources.Load<Sprite>("Items/" + itemName);

                if (spriteMain != null)
                {
                    item.SetImage(spriteMain);
                }

                item.canBeClicked = false;

                currentItemList.Add(item);
                item.gameObject.SetActive(false);
            }


        }
        StartCoroutine(_EnableMemory());

    }


    private void CheckWon()
    {
        bool checkWon = false;
        for (int i = 0; i < currentItemList.Count; i++)
        {
            if (!currentItemList[i].memory)
            {
                checkWon = false;
                return;
            }
            else
                checkWon = true;

        }

        if (checkWon && checkWinOnce)
        {
            checkWinOnce = false;
            Debug.Log("PLAYER WON");

            winScreens[currentIndexWinScreen].gameObject.SetActive(true);
            winScreens[currentIndexWinScreen].FadeToVisible();
            playAgainButton.gameObject.SetActive(true);
            if (currentIndexWinScreen == 0)
                currentIndexWinScreen = 1;
            else
                currentIndexWinScreen = 0;

        }
    }


    private void AddPoints()
    {
        int currentCount = int.Parse(pointCountText.text);
        int endCount = currentCount + 25;
        LeanTween.value(currentCount, endCount, 2f).setOnUpdate(UpdatePointCounter);

    }

    private void UpdatePointCounter(float obj)
    {
        pointCountText.text = obj.ToString("0");
    }


    IEnumerator _LostMemory()
    {
        startTimer = false;
        yield return new WaitForSeconds(1f);
        foreach (Match3DItem i in currentItemList)
        {
            if (i.gameObject.activeSelf)
            {
                i.canBeClicked = false;
                i.isClicked = false;
                i.gameObject.SetActive(false);
            }
            playAgainButton.gameObject.SetActive(true);

        }
    }

    public void IncorrectMatch()
    {
        CameraShake.Instance.ShakeOnce(0.5f, 4f);

        remainingTriesSlider.value++;
        int failsLeft = int.Parse(failureCountText.text) - 1;
        failureCountText.text = failsLeft.ToString();
        if (failsLeft <= 0)
        {
            StartCoroutine(_LostMemory());
        }
    }

    public void CorrectMatch(Match3DItem firstInTrigger, Match3DItem secondInTrigger)
    {
        AddPoints();
        firstInTrigger.memory = true;
        secondInTrigger.memory = true;
        StartCoroutine(_CorrectMatch(firstInTrigger, secondInTrigger));
    }

    IEnumerator _CorrectMatch(Match3DItem firstInTrigger, Match3DItem secondInTrigger)
    {
        LeanTween.scale(firstInTrigger.gameObject, Vector3.zero, 1f).setEaseOutBounce();
        LeanTween.scale(secondInTrigger.gameObject, Vector3.zero, 1f).setEaseOutBounce();
        yield return new WaitForSeconds(1f);
        dropZone.Clear();
        firstInTrigger.gameObject.SetActive(false);
        secondInTrigger.gameObject.SetActive(false);

        CheckWon();
    }
}
