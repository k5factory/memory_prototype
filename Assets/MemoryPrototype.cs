using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MemoryPrototype : MonoBehaviour
{
    // Start is called before the first frame update
    public string itemListName = "itemsTest";
    public int countOfMemoryItems = 12;
    public static MemoryPrototype Instance;
    [Header("References")]
    public GridLayoutGroup contentParent;
    public Button playAgainButton;

    public CanvasAlphaControl[] winScreens;
    public TextMeshProUGUI failureCountText;
    public Slider remainingTriesSlider;
    [Space]
    public TextMeshProUGUI timeCountText;
    public TextMeshProUGUI pointCountText;
    public GameObject[] starsParticles;

    public GameObject memoryPrefab;

    [Header("Controls")]
    public int failureCount = 10;

    public Transform[] correctPositions;

    List<ItemMemory> currentMemoryList = new List<ItemMemory>();


    TextAsset itemFile;

    private bool startTimer = false;
    private float currentTime = 0;

    private int currentIndexWinScreen = 0;

    private bool checkWinOnce = true;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);


    }

    private void Start()
    {
        remainingTriesSlider.value = 0;
        remainingTriesSlider.maxValue = failureCount;
        failureCountText.text = failureCount.ToString();
        playAgainButton.gameObject.SetActive(false);

        foreach (CanvasAlphaControl c in winScreens)
        {
            c.cGroup.alpha = 0f;
            c.gameObject.SetActive(false);
        }

        StartCoroutine(_CreateMemory());

    }

    private void Update()
    {
        if (startTimer)
        {
            currentTime += Time.deltaTime;
            float minutes = Mathf.FloorToInt(currentTime / 60);
            float seconds = Mathf.FloorToInt(currentTime % 60);
            timeCountText.text = minutes.ToString(("0")) + ":" + seconds.ToString(("00"));

        }
    }


    public void RestartMemory()
    {
        checkWinOnce = true;
        pointCountText.text = "0";
        startTimer = false;
        currentTime = 0;
        timeCountText.text = "0:00";
        foreach (ItemMemory i in currentMemoryList)
        {
            i.gameObject.SetActive(false);
            i.gameObject.transform.SetParent(null);
        }

        DisableEnableLayout(true);

        remainingTriesSlider.value = 0;
        failureCountText.text = failureCount.ToString();


        foreach (CanvasAlphaControl c in winScreens)
        {
            c.cGroup.alpha = 0f;
            c.gameObject.SetActive(false);
        }

        playAgainButton.gameObject.SetActive(false);

        StartCoroutine(_EnableMemory());


    }

    IEnumerator _EnableMemory()
    {
        currentMemoryList = ShuffleItemList(currentMemoryList);


        for (int i = 0; i < currentMemoryList.Count; i++)
        {

            currentMemoryList[i].ResetItem();
            currentMemoryList[i].gameObject.transform.SetParent(contentParent.transform);
            currentMemoryList[i].gameObject.transform.localScale = Vector3.one;
            currentMemoryList[i].gameObject.transform.localPosition = new Vector3(currentMemoryList[i].transform.localPosition.x,
                currentMemoryList[i].gameObject.transform.localPosition.y, 0f);
            currentMemoryList[i].gameObject.transform.localEulerAngles = new Vector3(0, 180f, 0);
            currentMemoryList[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);

        }

        foreach (ItemMemory i in currentMemoryList)
        {
            i.SwitchCardBack();
        }

        foreach (ItemMemory i in currentMemoryList)
        {
            i.SwitchCardBack();
        }

        yield return new WaitForSeconds(1f);
        foreach (ItemMemory i in currentMemoryList)
        {
            i.canBeClicked = true;
        }

        startTimer = true;
        yield return new WaitForSeconds(0.1f);
        DisableEnableLayout(false);

    }
    IEnumerator _CreateMemory()
    {
        itemFile = Resources.Load(itemListName) as TextAsset;

        yield return new WaitForSeconds(0.5f);
        string[] lines = itemFile.text.Split("\n"[0]);
        List<string> linesList = new List<string>(lines);

        // Lines are empty at that point
        linesList.Remove(linesList[0]);
        linesList.Remove(linesList[linesList.Count - 1]);
        //linesList.Remove(linesList[linesList.Count - 1]);

        Debug.Log("LINES COUNT: " + linesList.Count);

        List<string> pickedItemList = new List<string>();
        List<int> randList = new List<int>();
        int rand = 0;
        for (int i = 0; i < countOfMemoryItems; i++)
        {
            do
            {
                rand = Random.Range(0, linesList.Count);

            } while (randList.Contains(rand));

            randList.Add(rand);
        }
        for (int i = 0; i < randList.Count; i++)
        {
            pickedItemList.Add(linesList[randList[i]]);
        }


        for (int i = 0; i < pickedItemList.Count; i++)
        {

            // If Player has Item with ID in his Deck
            string[] dataValues = pickedItemList[i].Split(',');
            int currentItemID = int.Parse(dataValues[0]);

            for (int j = 0; j < 2; j++)
            {
                Debug.Log(" CURRENT ITEM" + pickedItemList[i]);

                ItemMemory item = Instantiate(memoryPrefab).GetComponent<ItemMemory>();

                item.transform.localScale = Vector3.one;
                string itemName = dataValues[1];
                item.nameItem = itemName;
                item.id = currentItemID;

                // Set Category CURRENTLY not necessary
                if (int.Parse(dataValues[2]) == 1)
                    item.GetComponent<ItemMemory>().mainCategory = ItemMemory.MainCategory.Food;
                else if (int.Parse(dataValues[2]) == 2)
                    item.GetComponent<ItemMemory>().mainCategory = ItemMemory.MainCategory.Music;
                else if (int.Parse(dataValues[2]) == 3)
                    item.GetComponent<ItemMemory>().mainCategory = ItemMemory.MainCategory.Love;
                else if (int.Parse(dataValues[2]) == 4)
                    item.GetComponent<ItemMemory>().mainCategory = ItemMemory.MainCategory.Living;
                else if (int.Parse(dataValues[2]) == 5)
                    item.GetComponent<ItemMemory>().mainCategory = ItemMemory.MainCategory.Fashion;

                var spriteMain = Resources.Load<Sprite>("Items/" + itemName);

                if (spriteMain != null)
                {
                    item.SetImage(spriteMain);
                }

                item.canBeClicked = false;

                currentMemoryList.Add(item);
                item.gameObject.SetActive(false);
            }


        }
        StartCoroutine(_EnableMemory());

    }


    public void DisableEnableLayout(bool enable)
    {
        contentParent.enabled = enable;

    }
    public List<ItemMemory> ShuffleItemList(List<ItemMemory> aList)
    {

        System.Random _random = new System.Random();

        ItemMemory myGO;

        int n = aList.Count;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(_random.NextDouble() * (n - i));
            myGO = aList[r];
            aList[r] = aList[i];
            aList[i] = myGO;
        }

        return aList;
    }

    public void MoveStarsParticle(Vector3 starsPositionOne, Vector3 starsPositionTwo)
    {
        starsParticles[0].transform.position = starsPositionOne;
        starsParticles[0].gameObject.SetActive(true);
        LeanTween.move(starsParticles[0], pointCountText.transform.position, 2f).setEaseInOutExpo().setOnComplete(MoveStarsCompleted);
        starsParticles[1].transform.position = starsPositionTwo;
        starsParticles[1].gameObject.SetActive(true);
        LeanTween.move(starsParticles[1], pointCountText.transform.position, 2f).setEaseInOutExpo().setOnComplete(MoveStarsCompleted);
    }

    private void MoveStarsCompleted()
    {
        int currentCount = int.Parse(pointCountText.text);
        int endCount = currentCount + 25;
        LeanTween.value(currentCount, endCount, 2f).setOnUpdate(UpdatePointCounter);

        foreach (var aParticle in starsParticles)
        {
            aParticle.gameObject.SetActive(false);
        }

    }

    private void UpdatePointCounter(float obj)
    {
        pointCountText.text = obj.ToString("0");
    }

    public void CorrectSelection(ItemMemory first, ItemMemory second)
    {

        StartCoroutine(_AnimateCorrectItems(first, second));
        StartCoroutine(_WaitForNextSelection());
    }

    private void CheckWon()
    {
        bool checkWon = false;
        for (int i = 0; i < currentMemoryList.Count; i++)
        {
            if (!currentMemoryList[i].memory)
            {
                checkWon = false;
                return;
            }
            else
                checkWon = true;

        }

        if (checkWon && checkWinOnce)
        {
            checkWinOnce = false;
            Debug.Log("PLAYER WON");

            winScreens[currentIndexWinScreen].gameObject.SetActive(true);
            winScreens[currentIndexWinScreen].FadeToVisible();
            playAgainButton.gameObject.SetActive(true);
            if (currentIndexWinScreen == 0)
                currentIndexWinScreen = 1;
            else
                currentIndexWinScreen = 0;

        }
    }

    IEnumerator _AnimateCorrectItems(ItemMemory first, ItemMemory second)
    {
        LeanTween.move(first.gameObject, correctPositions[0].position, 1f).setEaseInOutExpo();
        LeanTween.move(second.gameObject, correctPositions[1].position, 1f).setEaseInOutExpo();
        yield return new WaitForSeconds(1f);
        first.goldEdge.gameObject.SetActive(true);
        first.goldEdge.Play();
        second.goldEdge.gameObject.SetActive(true);
        second.goldEdge.Play();
        yield return new WaitForSeconds(.5f);
        MoveStarsParticle(first.transform.position, second.transform.position);
        yield return new WaitForSeconds(1f);
        first.goldEdge.gameObject.SetActive(false);
        second.goldEdge.gameObject.SetActive(false);
        first.DissolveCard();
        second.DissolveCard();
        yield return new WaitForSeconds(1f);
        first.gameObject.SetActive((false));
        second.gameObject.SetActive((false));

        CheckWon();
    }

    public void WrongSelection()
    {
        CameraShake.Instance.ShakeOnce(0.5f, 4f);

        remainingTriesSlider.value++;
        int failsLeft = int.Parse(failureCountText.text) - 1;
        failureCountText.text = failsLeft.ToString();
        StartCoroutine(_WaitForNextSelection());

        if (failsLeft <= 0)
        {
            StartCoroutine(_LostMemory());
        }
    }

    IEnumerator _LostMemory()
    {
        startTimer = false;
        yield return new WaitForSeconds(1f);
        foreach (ItemMemory i in currentMemoryList)
        {
            if (i.gameObject.activeSelf)
                i.FailedMemory();
        }
        playAgainButton.gameObject.SetActive(true);

    }

    IEnumerator _WaitForNextSelection()
    {
        foreach (ItemMemory i in currentMemoryList)
        {
            i.canBeClicked = false;
        }
        yield return new WaitForSeconds(1f);
        foreach (ItemMemory i in currentMemoryList)
        {
            i.canBeClicked = true;
            i.previousClicked = null;
        }
    }


    public void PowerupShowAll(float time)
    {
        StartCoroutine((_PowerupShowAll(time)));
    }
    IEnumerator _PowerupShowAll(float showTime)
    {

        CameraShake.Instance.ShakeOnce(1f, 2f);
        foreach (ItemMemory i in currentMemoryList)
        {
            i.SwitchCardFront();
            i.canBeClicked = false;
        }

        yield return new WaitForSeconds(showTime);
        foreach (ItemMemory i in currentMemoryList)
        {
            i.SwitchCardBack();
            i.canBeClicked = true;
        }
    }

}
