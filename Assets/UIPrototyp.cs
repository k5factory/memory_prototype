using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPrototyp : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("References")]
    public CanvasAlphaControl[] canvasesAlphaControls;

    public CanvasAlphaControl currentCanvas;

    private Coroutine switchCoroutine;

    public bool test = true;

    private int itemsMarked = 0;
    public GameObject activateFindPairsButton;
    private void Start()
    {
        activateFindPairsButton.SetActive(false);
        if (!test)
        {
            foreach (var c in canvasesAlphaControls)
            {
                c.gameObject.SetActive(true);
                c.cGroup.alpha = 0;
                c.gameObject.SetActive(false);
            }

            canvasesAlphaControls[0].gameObject.SetActive(true);
            currentCanvas = canvasesAlphaControls[0];
            currentCanvas.FadeToVisible();

        }
    }

    public void SwitchCanvas(int toCanvas)
    {
        switchCoroutine = StartCoroutine(_SwitchCanvas(toCanvas));
    }

    IEnumerator _SwitchCanvas(int toCanvas)
    {
        currentCanvas.FadeToInvisible();
        yield return new WaitForSeconds(currentCanvas.fadeTime);
        canvasesAlphaControls[toCanvas].gameObject.SetActive(true);

        canvasesAlphaControls[toCanvas].FadeToVisible();
        currentCanvas.gameObject.SetActive(false);
        currentCanvas = canvasesAlphaControls[toCanvas];
    }


    public void MarkItems()
    {
        itemsMarked++;

        if (itemsMarked >= 5)
        {
            activateFindPairsButton.gameObject.SetActive(true);
        }
    }

    public void DeselectItems()
    {
        itemsMarked--;
        if (itemsMarked < 5)
        {
            activateFindPairsButton.gameObject.SetActive(false);
        }
    }

    public void PlayFindPairs()
    {
        SceneManager.LoadScene(1);
    }
}
