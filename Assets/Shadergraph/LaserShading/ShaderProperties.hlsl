float3 _StartPositions[16];
float3 _EndPositions[16];
float3 _Colors[16];
float _RadiusList[16];
float _Radius2List[16];
int _LaserCount = 0;

float sdCapsule_float(float3 p, out float3 sdfColor)
{
	float sdf = 0.0f;
	for (int i = 0; i < _LaserCount; i++)
	{
		float3 pa = p - _StartPositions[i], ba = _EndPositions[i] - _StartPositions[i];
		float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
		sdf = (length(pa - ba * h) - _Radius2List[i]) / _RadiusList[i];
		sdf = max(0.0f, sdf);
		sdf = 1.0f/ (1.0 + sdf*sdf);
		sdfColor += float3(_Colors[i][0] * sdf, _Colors[i][1] * sdf, _Colors[i][2] * sdf);
	}
	
	return sdfColor;
}
