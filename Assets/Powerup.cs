using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class Powerup : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update
    public enum PowerupType { ShowAll, ResetFails, Time };

    public PowerupType powerupType;

    public int currentCount = 0;
    public float showAllTime = 3f;
    [Header("References")]
    public TextMeshProUGUI powerupCountText;

    private void Start()
    {
        UpdateCurrentCount(); ;
    }
    private void UpdateCurrentCount()
    {
        if (powerupCountText)
            powerupCountText.text = currentCount.ToString();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (powerupType == PowerupType.ShowAll && currentCount > 0)
        {
            currentCount--;
            UpdateCurrentCount();
            MemoryPrototype.Instance.PowerupShowAll(showAllTime);
        }
    }
}
